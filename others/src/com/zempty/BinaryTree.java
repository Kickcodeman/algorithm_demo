package com.zempty;

import org.junit.Test;

import java.util.LinkedList;

public class BinaryTree {


    private int data;

    private BinaryTree left;

    private BinaryTree right;

    public BinaryTree(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        data = data;
    }

    public BinaryTree getLeft() {
        return left;
    }

    public void setLeft(BinaryTree left) {
        this.left = left;
    }

    public BinaryTree getRight() {
        return right;
    }

    public void setRight(BinaryTree right) {
        this.right = right;
    }


    public static void main(String[] args) {

        BinaryTree node1 = new BinaryTree(16);
        BinaryTree node2 = new BinaryTree(13);
        BinaryTree node3 = new BinaryTree(20);
        BinaryTree node4 = new BinaryTree(10);
        BinaryTree node5 = new BinaryTree(15);
        BinaryTree node6 = new BinaryTree(22);
        BinaryTree node7 = new BinaryTree(21);
        BinaryTree node8 = new BinaryTree(26);


        node1.left=node2;
        node1.right = node3;
        node2.left = node4;
        node2.right = node5;
        node3.right = node6;
        node6.left = node7;
        node6.right = node8;

//        printBinaryTree(node1);
        preOrderTraverse(node1);
        System.out.println();
        inOrderTraverse(node1);
        System.out.println();
        postTraverse(node1);

    }


    public static void printBinaryTree(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return;
        }

        LinkedList<BinaryTree> queue = new LinkedList<>();
        queue.offer(binaryTree);
        while (queue.size() > 0) {
            BinaryTree binaryTree1 = queue.poll();
            System.out.println("当前的结点是： "+binaryTree1.getData());
            if (binaryTree1.left != null) {
                queue.offer(binaryTree1.left);
            }
            if (binaryTree1.right != null) {
                queue.offer(binaryTree1.right);
            }
        }
    }

    public static void preOrderTraverse(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return;
        }
        System.out.print(binaryTree.getData()+" ");
        preOrderTraverse(binaryTree.left);
        preOrderTraverse(binaryTree.right);
    }

    public static void inOrderTraverse(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return;
        }
        inOrderTraverse(binaryTree.left);
        System.out.print(binaryTree.getData()+" ");
        inOrderTraverse(binaryTree.right);
    }

    public static void postTraverse(BinaryTree binaryTree) {
        if (binaryTree == null) {
            return;
        }

        postTraverse(binaryTree.right);
        System.out.print(binaryTree.getData()+" ");
        postTraverse(binaryTree.left);
    }

    int [] a = {1,2,3,5};
    int [] b = new int[5];



}

