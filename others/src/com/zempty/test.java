package com.zempty;

import org.junit.Test;

import javax.xml.soap.Node;
import java.util.*;

/**
 * @author zempty
 * @ClassName test.java
 * @Description test
 * @createTime 2020年06月24日 23:16:00
 */
public class test {

    public static void main(String[] args) {
        int[] a = {1, 2, 3, 4, 5};
        int[] b = new int[5];
        int j = 0;
        for (int i = a.length - 1; i >= 0; i--) {
            b[j] = a[i];
            j++;
        }
        System.out.println(Arrays.toString(b));
    }


    @Test
    public void test1(){
        int[] a = {1, 2, 3, 4, 5, 6, 3, 6, 2, 3, 6, 1, 3};
        int tem_max = 0;
        int max_amount = 0;
        for (int i = 0; i < a.length-1; i++) {
            int tem_amount = 0;
            for (int j = 0; j <a.length-1 ; j++) {
                if (a[i] == a[j]) {
                    tem_amount += 1;
                }
                if (tem_amount > tem_max) {
                    tem_max = tem_amount;
                    max_amount = a[i];
                }
            }
        }
        System.out.println(max_amount);
    }

    @Test
    public void test2() {
        Queue<String> q = new ArrayDeque();
        int[] a = {1, 2, 3, 4, 5, 6, 3, 6, 2, 3, 6, 1, 3};
        int amount_max = 0;
        int data_amount_max = 0;
        HashMap<Integer,Integer> map = new HashMap();
        for (int i = 0; i < a.length; i++) {
            if (map.containsKey(a[i])) {
                map.put(a[i], map.get(a[i])+1);
            } else {
                map.put(a[i], 1);
            }
        }

        Set<Integer> datas = map.keySet();
        for (Integer data : datas) {
            if (map.get(data) > amount_max) {
                amount_max = map.get(data);
                data_amount_max = data;
            }
        }
        System.out.println("出现次数最多的数是："+ data_amount_max + " 出现的次数是：" + amount_max);

    }


    @Test
    public void test() {
        int a = 5;
        int b = 0;
        b = a;
        System.out.println(a);
        System.out.println(b);
    }

    @Test
    public void test23() {
        int a = 10;
        int b = 6;
        System.out.println(Math.max(a,b));
    }
}
