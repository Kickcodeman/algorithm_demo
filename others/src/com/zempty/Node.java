package com.zempty;

import jdk.internal.org.objectweb.asm.util.CheckAnnotationAdapter;

public class Node {

    private Node next;

    private int data;

    public Node(int data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public static void main(String[] args) {

        Node node1 = new Node(1);
        Node node2 = new Node(2);
        Node node3 = new Node(3);
        Node node4 = new Node(4);
        Node node5 = new Node(5);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);
        node4.setNext(node5);
        printNode(node1);

//        swapNode(node1);
        System.out.println("==================");
//        Node newHead = swapNode(node1);
//        System.out.println(newHead.data);
//        printNode(newHead);
        Node node = swapNode2(node1);
        printNode(node);



    }

    public static void printNode(Node node) {

        while (node != null) {
            System.out.print(node.getData()+" ");
            node = node.next;
        }

    }

    public static Node swapNode(Node node) {
        if (node == null ||node.getNext() == null) {
            return node;
        }
        Node newHead = swapNode(node.getNext());
        node.getNext().setNext(node);
        node.setNext(null);
        return newHead;

    }

    public static Node swapNode2(Node node) {

        Node prev = null;
        Node current = node;
        Node next = null;

        while (current!= null) {
            next = current.next;
            current.next = prev;
            prev = current;
            current = next;
        }
        return prev;

    }



}
