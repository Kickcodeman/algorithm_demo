package com.zempty;

import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

/**
 * @author zempty
 * @ClassName StringTest.java
 * @Description TODO
 * @createTime 2020年06月27日 12:11:00
 */

public class StringTest {


    @Test
    public void test1() {
        String s1 = "1234546546";
        String s2 = "34";
        int find = 0;
        for (int i = 0; i < s1.length() - s2.length()+1; i++) {
            for (int j = 0; j < s2.length(); j++) {
                if (s1.charAt(i+j) != s2.charAt(j)) {
                    break;
                }
                if (j == s2.length()-1) {
                    find = 1;
                }
            }
        }

        if (find > 0) {
            System.out.println("s2 是 s1 的字串");
        }

    }


    @Test
    public void test2() {
        String s1 = "l;kgjasljgalskjg;lakjsglkas";
        String s2 = "lghlagkjlksajdgksajdgksadjglk";
        String max_string = null;
        int max_length = 0;

        for (int i = 0; i < s2.length(); i++) {
            for (int j = 0; j < s1.length(); j++) {
                if (s2.charAt(i) == s1.charAt(j)) {
                    for (int m = i, n = j; m < s2.length() && n < s1.length(); m++, n++) {
                        if (s2.charAt(m) != s1.charAt(n)) {
                            break;
                        }
                        //这里是难点
                        if (max_length < m - i + 1) {
                            max_length = m - i + 1;
                            max_string = s2.substring(i, m + 1);
                        }
                    }

                }
            }
            
        }
        System.out.println("公共字符串的最大长度是："+ max_length + "; 公共字符串的是："+ max_string);
    }

    @Test
    public void test3() {
        int a[] = {1, 2, 3, 4, -5, -1, -6, 3, 2};

        Arrays.sort(a);
        Arrays.stream(a).forEach(System.out::print);
        int sum = 0;
        int sum1 = 0;
        int sum2 = 0;
        int length = a.length;

        if (a[length - 1] < 0) {
            sum = a[length - 1] * a[length - 2] * a[length - 3];
        }

        sum1 = a[0] * a[1] * a[length - 1];
        sum2 = a[length - 1] * a[length - 2] * a[length - 3];
        sum = sum1 > sum2 ? sum1 : sum2;
        System.out.println();
        System.out.println(sum);
        int test = new Random().nextInt(10);
        System.out.println();
        System.out.println(test);





    }


    @Test
    public void test() {
        String a = "abxdefghijk";
        System.out.println(a.codePointAt(0));
        System.out.println(a.codePointBefore(1));
        System.out.println(a.codePointCount(1,8));
        int x = a.offsetByCodePoints(1, 2);
        System.out.println(a.offsetByCodePoints(1,2));
        System.out.println(a.codePointAt(1));
        System.out.println(a.codePointAt(3));
        System.out.println(a.codePointAt(x));


    }

//    给定一个字符串，逐个翻转字符串中的每个单词。例如，输入: "the sky is blue"，输出: "blue is sky the"。
//    public void
}
