package com.zempty.array;

import org.junit.Test;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @author zempty
 * @ClassName TestDemo.java
 * @Description TODO
 * @createTime 2021年03月31日 22:18:00
 */
public class TestDemo {
    public static void main(String[] args) {

//        Scanner in = new Scanner(System.in);
//        System.out.println("请输入一个整数：");
//        int a = in.nextInt();
//        System.out.println("a 是："+a);
//        System.out.println("开始输入一行数据 str1：");
//        String str1 = in.nextLine();
//        System.out.println("str1 被跳过了。。。。。。");
//        System.out.println("str1 是："+str1);
//        System.out.println("开始输入一行数据 str2：");
//        String str2 = in.nextLine();
//        System.out.println("str2 是："+str2);


//        Scanner s = new Scanner(System.in);
//        System.out.println("你最爱的动物是什么？");
//        String name = s.nextLine();
//        System.out.print("你最爱的动物是："+name );
//        //会打包带走换行符,但是不会包含在字符串里面！
////        System.out.println("——————————————————————");
//        //如果包含在了name中，那么会换行输出，但是并没有！
//
////        System.out.println("This is a text!");
//        String text = s.nextLine();
//        System.out.println("Test results:"+text);
        //因为nextLine()会带走换行符，所以text需要键盘输入


//        Scanner s = new Scanner(System.in);
//        System.out.println("你最爱的动物是什么？");
//        String name = s.next();
//        System.out.println("你最爱的动物是："+name);
//        //就算输入的时候加上了很多的空格，但是被忽略了！

//        System.out.println("你最爱的动物是什么？");
//        String name = s.next();
//        System.out.println("你最爱的动物是：" + name);
//
//        String name1 = s.next();
//        System.out.println("你最爱的动物是：" + name1);
//
//        String name2 = s.next();
//        System.out.println("你最爱的动物是：" + name2);


//        Scanner in = new Scanner(System.in);
//        String s = "";
//        String bold = "";
//        int fromIndex = 0;
//        String s1 = in.nextLine();
//        String s2 = in.nextLine();
//        String[] a = null;
//        System.out.println("s1============================");
//        System.out.println(s1);
//        a = stringToArray(s1);
//        System.out.println("s2============================");
//        System.out.println(s2);
//        if (a != null&&a.length>0) {
//            for (String s3 : a) {
//                s = s + s3;
//            }
//        }
//        System.out.println("s=======================");
//        System.out.println(s);
////        for(int)
//        for (int i = 0; i < s2.length() - 1; i++) {
//            for (int j = 0; j < s.length() - 1; j++) {
//                if (s2.charAt(i) == s.charAt(j)) {
//                    fromIdex = i;
//                    bold = bold + String.valueOf(s2.charAt(i));
//                }
//                break;
//            }
//        }

    }


    @Test
    public void test() {
        String s = "1,4,3,4,5,6";
        String b = "2,7,4,5,5";
        s = s.replace(",", "");
        b = b.replace(",", "");
        s = s + b;
        System.out.println(s.length());
        int[] a = new int[s.length()];
        for(int i = 0;i<s.length();i++){
            char y = s.charAt(i);
            Integer test = y -'0';
            System.out.println("=======================");
            System.out.println(test);
            Integer x =Integer.valueOf(String.valueOf(s.charAt(i)));
            a[i] = x;
        }
        Arrays.sort(a);
        System.out.println(Arrays.toString(a).replace("[","").replace("]",""));
    }


    @Test
    public void demo() {
        String s1 = "hello,world,ni,hao,zhong,guo";
        String s = "zhongguo";
        String s2 = s1.replace(",","");
        System.out.println(s2);
        System.out.println(s2.contains(s));
    }



    @Test
    public void contailString() {
        Scanner in = new Scanner(System.in);
        String s = null;
        String s1 = in.nextLine();
        String s2 = in.nextLine();
        String[] a = null;
        System.out.println("s1============================");
        System.out.println(s1);
        a = stringToArray(s1);
        System.out.println("s2============================");
        System.out.println(s2);
        if (a != null&&a.length>0) {
            for (String s3 : a) {
                s = s + s3;
            }
        }
        System.out.println("s=======================");
        System.out.println(s);
//        for(int)
    }


    private static String[] stringToArray(String s) {
        String[] array = null;
        if (s.contains(",")) {
            array = s.split(",");
        }
        return array;
    }


    @Test
    public void increase() {
        int a = 1,b,c;
        b= a++;
        System.out.println(a);
        System.out.println(b);
        c=++a;
        System.out.println(a);
        System.out.println(c);
    }


    @Test
    public void forTest() {
        for (int i = 1; i < 0; i--) {
            System.out.println(i);
        }
    }


    @Test
    public void sortArray() {
        int[] a = new int[5];
        for (int i = 0;i<a.length;i++) {
            a[i] = (int) (Math.random() * 100);
        }
        System.out.println(Arrays.toString(a));

        for (int i = 0; i < a.length-1; i++) {
            for (int j = 0; j < a.length - 1-i; j++) {
                if(a[j] > a[j + 1]) {
                    int temp = a[i];
                    a[i] = a[j + 1];
                    a[j+1] = a[j];
                }
            }
        }
        System.out.println(Arrays.toString(a));
//        Arrays.fill(a,2);
        System.out.println(Arrays.toString(a));
    }


    @Test
    public void testPop() {
        int[] a = new int[100];
        for (int i = 0;i<a.length;i++) {
            a[i] = (int) (Math.random() * 100);
        }
        System.out.println(Arrays.toString(a));
        boolean flag = true;
        while (flag) {
            int temp = 0;
            int j = 0;
            int i = 0;
            while(i<a.length-1){
                if (a[i] > a[i + 1]) {
                    temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                    j++;
                }
                i++;
            }
            if (j == 0) {
                flag = false;
            }
        }
        System.out.println(Arrays.toString(a));

    }

    @Test
    public void absTest() {
        double d =-19.8d;
        double e = Math.ceil(d);
        System.out.println(e);
    }


    @Test
    public void testTemp() {
        int temp = 1;
        int a = 2;
        int b = 3;
        a = temp;
        System.out.println(a);
        b = a;
        System.out.println(temp);
        System.out.println(b);
    }

}