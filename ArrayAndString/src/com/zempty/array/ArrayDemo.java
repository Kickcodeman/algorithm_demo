package com.zempty.array;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ArrayDemo {

    private int[] nums;
    private int[][] arrays;

    @Before
    public void init() {
        // 提前准备测试的数组
        nums = new int[]{9};
        arrays = new int[][]{{1, 2,3}, {4, 5,6},{7,8,9},{7,8,9},{7,8,9}};
    }

    //测试数组中的一个元素比其他元素（和自己相同的除外）的两倍还要大
    @Test
    public void test() {
        //测试demo，可随便举例测试
        int max = 0,maxIndex = 0;
        // 查找数组中的最大值和最大值的索引
        for(int i = 0;i<=nums.length-1;i++){
            if(nums[i]>max){
                max = nums[i];
                maxIndex = i;
            }
        }

        //循环遍历，不符合条件就返回 -1
        for(int i :nums){
            if(i!=max && max<2*i) {
                System.out.println("不满足条件");
                return;
            }
        }
        System.out.println(maxIndex);
    }



    //把数组想像成一个整数，加一后输出一个新的数组(数组元素范围是[0,9])
    @Test
    public void plusOne() {
        for (int i = nums.length-1; i < 0; i--) {
            if (nums[i] < 9) {
                nums[i]++;
                printArray(nums);
                return;
            }else
                nums[i] = 0;
        }

        int newNums[] = new int[nums.length + 1];
        newNums[0] = 1;
        printArray(newNums);
    }


    public void printArray(int[] array) {
        Arrays.stream(array)
                .forEach(System.out::print);
    }



    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入文字：");
        String s = scanner.next();
        System.out.println(s);
    }



    public void printList(List<Integer> list) {
        list.forEach(System.out::print);
    }


    //二维数组的研究
    @Test
    public void testArray2D() {
        int[][] array = new int[][]{{1, 2, 3}, {4, 5, 6}};
        System.out.println(array[1][2]);
        System.out.println(new int[0]);
    }


    @Test
    public void testArray() {
        int r = 0, c = 0, rLength = arrays.length,cLength = arrays[0].length;
        int[] array = new int[rLength * cLength];
        if(arrays.length==0)
            return ;
        for (int i = 0; i < array.length; i++) {
            array[i] = arrays[r][c];
            //处理 r 和 c : r+c 是偶数
            if ((r + c) % 2 == 0) {
                if (c==cLength-1)
                    r++;
                else if(r == 0)
                    c++;
                else {
                    r--;
                    c++;
                }
            }else{
                if(r==rLength-1)
                    c++;
                else if(c==0)
                    r++;
                else{
                    r++;
                    c--;
                }
            }
        }
        printArray(array);
    }


    @Test
    public void spiralOrder() {

        List outputs = new ArrayList();
        int r = 0, length = arrays.length;
        int c = 0, columns = arrays[0].length;

        while (r <= length-1 && c <= columns-1) {

            //兼容了一行或者是一列的时候的情况，添加数列的第一行
            for (int i = c; i<= columns- 1; i++)
                outputs.add(arrays[r][i]);

            // 添加最后一列
            for(int i = r+1 ; i<=length-1;i++)
                outputs.add(arrays[i][columns-1]);


            //超过一行一列
            if (r < length - 1 && c < columns - 1) {
                for(int i = columns -2; i>=c; i--)
                    outputs.add(arrays[length-1][i]);
                for(int i = length -2;i>r;i--)
                    outputs.add(arrays[i][c]);

            }

            length--;
            columns--;
            r++;
            c++;
        }

        printList(outputs);


//
//        for(int i = 0; i<matrix.length ;i++){
//            // 处理第一行
//            if(i==0){
//                for(int j=0;j<matrix[0].length;j++)
//                    outputs.add(matrix[0][j]);
//            }
//            //2.最后一行从后往前进行遍历
//            if(i==matrix.length-1){
//                for(int j = matrix[i].length-1; j>=0 ; j--)
//                    outputs.add(matrix[i][j]);
//            }
//        }
//
//        //3 其他行遍历由下往上依次执行，注意坐标的移动
//        for(int i = matrix.length -2; i>0 ; i--){
//            for(int j= 0 ; j<= matrix[i].length-2 ; j++)
//                outputs.add(matrix[i][j]);
//        }


    }


    @Test
    public void testScanner() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        System.out.println("=======================");
        System.out.println(a);
    }


    @Test
    public void array() {
        int[] a = {1, 3, 1, 2, 4, 3, 5, 6};
        int temp = 0;
        for (int i = 0; i < a.length; i++) {
            if (i + 1 <= a.length-1)
                if (a[i] > a[i+1]) {
                    temp = a[i];
                    a[i] = a[i + 1];
                    a[i+1] = temp;
                }
            }
        Arrays.stream(a).forEach(System.out::print);
        }



    @Test
    public void initArray() {
        int [] a;
        a = new int[]{1, 2};
        System.out.println(a);

        int[] b = new int[5];

        System.out.println(b);
        int[] c = {1, 5};
        System.out.println(c);

        String s = "hello world";
        char x = s.charAt(0);
        System.out.println(x);

        char[] chars = s.toCharArray();
        for (char z : chars) {
            System.out.println(z);
        }
    }


    @Test
    public void testPlain() {
        String a = "fabccbaf";
        char[] b = new char[a.length()];
        for (int i = a.length() - 1; i >= 0; i--) {
            b[a.length() - 1 - i] = a.charAt(i);
        }
        if (a.equals(new String(b))) {
            System.out.println("该字符串是回文字符串");
        }
    }



    @Test
    public void s1() {

        String s = "goodgoogle";

        String t = "google";

        int isfind = 0;




        // 这个是多出来的长度
//        s.length() - t.length() +1 遍历到不足的长度，就不用遍历了
        System.out.println(s.length()-t.length());

        for (int i = 0; i < s.length() - t.length() + 1; i++) {

            if (s.charAt(i) == t.charAt(0)) {

                int jc = 0;

                for (int j = 0; j < t.length(); j++) {

                    if (s.charAt(i + j) != t.charAt(j)) {

                        break;

                    }

                    jc = j;

                }

                if (jc == t.length() - 1) {

                    isfind = 1;

                }

            }

        }

        System.out.println(isfind);

    }



    @Test
    public void isPalindrome() {
        Scanner scanner = new Scanner(System.in);
        String s = scanner.next();
        int x = 13;
        char[] chars = intToChar(x);
        System.out.println(checkPalindrome(chars));
    }


    private char[] intToChar(int x){
        String s = x + "";
        char[] chars = s.toCharArray();
        return chars;
    }


    private boolean checkPalindrome(char[] chars){
        char[] a = new char[chars.length];
        if(chars.length==0){
            return false;
        }
        if(chars.length==1){
            return true;
        }
        for(int i = 0;i<chars.length;i++){
            a[chars.length-i-1] = chars[i];
        }
        return new String(chars).equals(new String(a));
    }


    @Test
    private void absTest() {
        double d =-19.8d;
        double e = Math.ceil(d);
        System.out.println(e);
    }
}


 class Solution {
    public int[] findDiagonalOrder(int[][] matrix) {
        int r = 0, c = 0, rLength = matrix.length,cLength = matrix[0].length;
        int[] array = new int[rLength * cLength];
        for (int i = 0; i < array.length; i++) {
            array[i] = matrix[r][c];
            //处理 r 和 c : r+c 是偶数向上
            if ((r + c) % 2 == 0) {
                //该条件最先判断
                if (c==cLength-1)
                    r++;
                else if(r == 0)
                    c++;
                else {
                    r--;
                    c++;
                }

                //向下
            }else{
                //该条件最先判断
                if(r==rLength-1)
                    c++;
                else if(c==0)
                    r++;
                else{
                    r++;
                    c--;
                }
            }
        }
        return array;
    }









 }

