package com.zempty;

/**
 * @Classname ArrayQueue
 * @Description 用数组来实现队列
 * @Date 8/8/22 10:05 PM
 * @Created by zempty
 */
public class ArrayQueue {
    // 用来存储数组
    private String[] items;

    //用来表示队列的长度
    private int n;

    private int head = 0;
    private int tail = 0;

    public ArrayQueue(int capacity) {
        items = new String[capacity];
        n = capacity;
    }

    //入队操作
    public boolean enqueue(String item) {
        // 如果没有空间了，重新调整一下数组的数据
        if (tail == n) {
            if (head == 0) {
                return false;
            }
            for (int i = head; i < tail; i++) {
                items[i - head] = items[i];
            }
            head = 0;
            tail -= head;
        }
        items[tail] = item;
        tail ++;
        return true;
    }

    //出队操作
    public String dequeue() {
        if (tail == n) {
            return null;
        }
        String item = items[head];
        head++;
        return item;

    }
}
