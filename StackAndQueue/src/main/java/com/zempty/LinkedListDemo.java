package com.zempty;


//链表反转的代码案例
public class LinkedListDemo {

    // 链表反转
    // 1-> 2 ->3 ->4 -> 5
    // 第一轮通过递归反转链表结果是 :5-> 4 ->3 ->2 ->1
    // 第二轮通过指针移动反转链表重回结果: 1-> 2 ->3 ->4 -> 5
    public static void main(String[] args) {

        Node node = getNode();
        Node newNode = recursionNode(node);
        System.out.println(newNode);
        Node resetNode = changPointAndMove(newNode);
        System.out.println(resetNode);

    }

    //递归反转链表的方法
    private static Node recursionNode(Node node){

        // 递归的终止条件，只有一个 node 或者到最后一个 node 的时候终止递归
        if (node == null || node.getNext() == null) {
            return node;
        }
        /*这里开始进行递归，不断往后传递，直到到最后一个节点的时候
        * 这里需要返回最后一节点，做为反转链表后的起始节点
        * */
        Node newHead = recursionNode(node.getNext());
        node.next.next = node;
        node.next = null;
        return newHead;
    }


    //通过移动指针实现链表的一个反转
    private static Node changPointAndMove(Node node) {
        Node curr = node;
        Node prev = null;
        while (curr != null) {
            //下获取下一个节点
            Node next = curr.next;
            //改变指针
            curr.next = prev;

            //移动节点
            prev = curr;
            curr = next;
        }
        //循环跳出的条件是 curr == null ,此时 prev 就是最后一个节点，也就是反转后的起始节点
        return prev;
    }


    // 构建一个测试链表
    private static Node getNode() {
        Node node1 = new Node();
        node1.setVal(1);
        Node node2 = new Node();
        node2.setVal(2);
        Node node3 = new Node();
        node3.setVal(3);
        Node node4 = new Node();
        node4.setVal(4);
        Node node5 = new Node();
        node5.setVal(5);
        node1.setNext(node2);
        node2.setNext(node3);
        node3.setNext(node4);
        node4.setNext(node5);
        node5.setNext(null);
        return node1;
    }

}

//自定义一个链表
class Node {

    public Node next;

    public int val;

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val = val;
    }
}
