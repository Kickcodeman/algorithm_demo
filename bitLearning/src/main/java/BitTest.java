import org.junit.Test;

/*
* java 的位运算主要操作的是整数，运算结果也是整数
* java 位运算操作符号： & , ｜ , ^ , ~ , >> , << , >>>
* */test

public class BitTest {


    // java 的八种基本类型数据
    @Test
    public void primitiveType() {

        //四个整型数据
        // byte 占一个字节范围是 -128-127
        byte a = 1;

        // 占用两个字节 2 个字节 -2^15 - 2^15 - 1（-32768 - 32767）
        short b = 1;

        // 占用四个字节 -2^31 -2 ^31-1 (-2,147,483,648-2 - 147,483,647)
        int c = 1;

        //占用八个字节 -2^63 - 2^63-1(-9,223,372,036,854,775,808-9,223,372,036,854,775,807)
        long d = 1L;

        //浮点数
        //占用四个字节，单精度
        float e = 1.0f;

        //占用 8 个字节，双精度
        double f = 1.0d;

        //字符类型
        //占用两个字节,用单引号表示
        char g = 'c';
        //下面是一个很有意思的转换过程,'0'代表整型数据 48， 'c' 是 99
        int h = g - '0';
        System.out.println(h);
        long i = g - 0L;
        System.out.println(i);


        //boolean 类型数据: true/false
        boolean j = true;
        boolean k = false;

    }

    // & , | , ^ , ~ , >> , << , >>> 位运算操作整型数据，结果也是整型数据
    @Test
    public void bitTest() {
        int a = 3; // 00000000 00000000 00000000 00000011
        int b = 6; // 00000000 00000000 00000000 00000110

        //参与运算的位都是 1 的时候，该位才是 1 ,其余都是 0，所以结果是 2；
        int c = a & b; // 00000000 00000000 00000000 00000010

        // 参与运算的位有 1，结果才是 1，其余都是 0，所以结果是 7
        int d = a | b; //00000000 00000000 00000000 00000111

        //参与运算的位，如果相同值取 0 ，不同值取 1，所以结果是 5；
        int e = a ^ b; //00000000 00000000 00000000 00000101；

        // 3 按位取反获取到结果，计算机最高位表示符号位，最高位是 1 表示负数，负数是用补码表示的，补码减1可以获取反码，反码取反获取到源码
        //补码 11111111 11111111 11111111 111111100 计算机的表示形式
        //返码 11111111 11111111 11111111 111111101
        //源码 10000000 00000000 00000000 000000010 所以结果是 -4
        int f = ~3 ; //11111111 1111111 1111111 111111100
        System.out.println("3 的取反操作是：" +f);

        //相当于 3/2 = 1
        int g = 3 >> 1;

        //相当于 6 * 2 = 12
        int h = 6 << 1;

        //无符号相右移动，左边补 0
        int i = 3 >>> 1;
        System.out.println(i);
    }


    //奇偶判断，奇淫技巧
    //当一个偶数和 1 相 & 的时候 ，结果是 0
    //当一个奇数和 1 相 & 的时候，结果是 1；
    @Test
    public void oddAndEvenNumber() {
        int a = 3;
        int b = 6;

        int c = a & 1; // c = 1 所以 a 是奇数
        int d = b & 1; // d = 0 所有 b 是偶数

        if (c == 1) {
            System.out.println(a + " 是奇数");
        }
        if (d == 0) {
            System.out.println(b + " 是偶数");
        }
    }

    //奇淫技巧
    //值交换 x ^ x ^ y = y 结论是： 一个数异或自己再异或另一个数，结果是另一个数
    @Test
    public void swapValue() {
        int a = 3;
        int b = 6;
        int c = a ^ a ^ b; //结果是 6
        int d = b ^ b ^ a; //结果是 3
        System.out.println(c);
        System.out.println(d);
        //顺序是 aba 规则，等于号右边赋值操作都是 a ^ b
        a = a ^ b; // a = 3 ^ 6
        b = a ^ b; //此时相当于 3 ^ 6 ^ 6 = 3
        a = a ^ b; //此时相当于 3 ^ 6 ^ 3 = 6
        System.out.println(a);
        System.out.println(b);
    }

    @Test
    // 使用 & 运算符代替 % 取余操作
    //这里有一个要求就是除数或者参与与运算的数必须是 2 的幂次方，这样可以有这样的结论：
    // 1：参与运算的两个数 m,n , 被除数是 m ,除数是 n ,求余数；
    // 2：如果 n 是 2 的幂次方，那么就有 m % n = m & (n-1)
    public void modAndDivider() {
        int a = 7;
        int b = 2;
        int c = 3;
        int k = a & (b-1);
        int l = a % b;
        int m = a & (c - 1);
        int n = a % c;
        System.out.println(k == l);// true
        System.out.println(m == n);//false
    }
}