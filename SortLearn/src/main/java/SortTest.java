import org.junit.Test;

import java.util.Arrays;

public class SortTest {

    public int[] array = {5, 6, 2, 3, 7, 9,2,1 };



    @Test
    // 冒泡排序算法
    public void BubbleSort() {
        int length  = array.length;
        boolean flag = false;
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                    flag = true;
                }
            }
            if (!flag) {
                break;
            }
        }
        System.out.println(Arrays.toString(array));
    }


    // 插入排序算法
    @Test
    public void insertionSort1() {
        int length = array.length;
        for (int i = 1; i < length; i++){
            int value = array[i];
            int j = i - 1;
            for (; j >= 0; j--) {
                if (array[j] > value) {
                    array[j + 1] = array[j];
                }else{
                    break;
                }
            }
            array[j + 1] = value;
        }
        System.out.println(Arrays.toString(array));
    }


    @Test
    public void insertionSort2() {
//        5, 6, 2, 3, 7, 9,2,1
        int length = array.length;
        for (int i = 1; i < length; i++) {
            int value = array[i];
            //用来插入数据
            boolean flag = false;
            int j = 0;
            for (; j < i; j++) {
                if (array[j] > value) {
                    //向后移动数组,这里是代码的关键部分，不好理解
                    flag = true;
                    int k = i - 1;
                    for (;k >=j ; k--) {
                        array[k + 1] = array[k];
                    }
                    break;
                }
            }
            if (flag) {
                array[j] = value;
            }
        }
        System.out.println(Arrays.toString(array));
    }



    //选择排序算法
    @Test
    public void SelectionSort1() {
        int length = array.length;
        int i = 1;
        while (i < length) {
            int min = array[i];
            int index = i;
            for (int j = i; j < length; j++) {
                min = Math.min(min, array[j]);
                if (min == array[j]) {
                    index = j;
                }
            }
            if (array[i - 1] > min) {
                array[index] = array[i - 1];
                array[i - 1] = min;
            }
            i++;
        }
        System.out.println(Arrays.toString(array));

    }



    @Test
    public void selectionSort2() {
        int length = array.length;
        for (int i = 0; i < length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
        System.out.println(Arrays.toString(array));
    }


    @Test
    //归并排序算法（Merge Sort)
    public void mergeSort() {
        int start = 0;
        int end = array.length-1;
        splitSort(array, start, end);
        System.out.println(Arrays.toString(array));
    }

    private void splitSort(int[] array, int start, int end) {
        if (start>= end) {
            return;
        }
        int mid = (start + end)/2;
        splitSort(array, start, mid);
        splitSort(array, mid + 1, end);
        merge(array, start,mid, end);
    }

    private void merge(int[] nums, int start,int mid, int end) {
        int[] temp = new int[end - start +1];
        int left = start;
        int right = mid + 1;
        int index = 0;
        while (left <= mid && right <= end) {
            if (array[left] <= nums[right]) {
                temp[index++] = nums[left++];
            }else{
                temp[index++] = nums[right++];
            }
        }

        int i = left;
        int j = mid;
        if (right <= end) {
            i = right;
            j = end;
        }

        while (i <= j) {
            temp[index++] = nums[i++];
        }

        for (int m = 0; m < temp.length; m++) {
            nums[start + m] = temp[m];
        }

    }


    @Test
    public void quickSort() {
        int start = 0;
        int end = array.length-1;
        sort(array, start, end);
        System.out.println(Arrays.toString(array));
    }

    private void sort(int[] array, int start, int end) {
        if (start >= end) {
            return;
        }
        int index = partition( array, start, end);
        sort(array, start, index-1);
        sort(array, index + 1, end);
    }

    private int partition(int[] array, int start, int end) {
        int num = array[end];
        int i = start;
        int temp = 0;
        for (int j = i; j < end; j++) {
            if (array[j] < num) {
                temp = array[j];
                array[j] = array[i];
                array[i] = temp;
                i++;
            }
        }
        temp = array[i];
        array[i] = num;
        array[end] = temp;
        return i;
    }

}
