import org.junit.jupiter.api.Test;

public class BinarySearchTest {

    //有序数组，查找值等于 3 的数组位置
    private int[] nums = {1,3,3,3,3,5,5,5,5,6,7,9};
//    private int[] nums = {5};


    // 1. 查找值等于某个值
    @Test
    public void binarySearch() {
        int target = 3;
        int length = nums.length;
        int left = 0;
        int end = length - 1;
        while (left <= end) {
            int mid = left + (end - left) / 2;
            if (nums[mid] == target) {
                System.out.println("数组索引位置是 " + mid + " 要查找的只是 " + nums[mid]);
                return;
            } else if (nums[mid] > target) {
                end = mid -1;
            }else{
                left = left + 1;
            }
        }
    }



    //==========================================
    //2.查找第一个值等于给定值的元素
    @Test
    public void binarySearchFirst1() {
        int target = 3;
        int left = 0;
        int end = nums.length - 1;
        while (left <= end) {
            int mid = left + ((end-left)>>1);
            if (nums[mid] >= target) {
                end = mid - 1;
            }else{
                left = mid + 1;
            }
        }
        if (left < nums.length && nums[left] == target) {
            System.out.println("数组索引位置是 " + left + " 要查找的只是 " + nums[left]);
        } else {
            System.out.println("没有找到");
        }
    }


    @Test
    public void binarySearchFirst2() {
        int target = 3;
        int left = 0;
        int end = nums.length - 1;
        while (left <= end) {
            int mid = left + ((end - left) >> 1);
            if (nums[mid] > target) {
                end = mid - 1;
            } else if (nums[mid] < target) {
                left = mid + 1;
            }else{
                if (mid == 0 || nums[mid-1]< target) {
                    System.out.println("数组索引位置是 " + mid + " 要查找的只是 " + nums[mid]);
                    return;
                }else{
                    end = mid - 1;
                }
            }
        }
    }



    //==========================================

    @Test
    //查找最后一个值等于给定值的元素
    public void binarySearchLast1() {
        int target = 3;
        int left = 0;
        int end = nums.length - 1;
        while (left <= end) {
            int mid = left + ((end - left) >> 1);
            if (nums[mid] <= target) {
                left = mid + 1;
            }else{
                end = mid - 1;
            }
        }
        if (end >= 0 && nums[end] == target) {
            System.out.println("数组索引位置是 " + end + " 要查找的只是 " + nums[end]);
        }
    }


    @Test
    public void binarySearchLast2() {
        int target = 3;
        int left = 0;
        int length = nums.length;
        int end = length - 1;
        while (left <= end) {
            int mid = left + ((end - left) >> 1);
            if (nums[mid] > target) {
                end = mid - 1;
            }else if (nums[mid] < target) {
                left = mid + 1;
            }else{
                if(mid == length - 1 || nums[mid + 1] > target){
                    System.out.println("数组索引位置是 " + mid + " 要查找的只是 " + nums[mid]);
                    return;
                }else{
                    left = mid + 1;
                }
            }
        }
    }


    //=====================================

    //查找第一个大于等于给定值的元素
    // 处理方式同 binarySearchFirst1 相同
    @Test
    public void binarySearchMoreThanOrEqualFirst() {
        int target = 4;
        int length = nums.length;
        int right = length - 1;
        int left = 0;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if(nums[mid] >= target){
                right = mid - 1;
            }else{
                left = mid + 1;
            }
        }
        if (left >= 0 && nums[left] >= target) {
            System.out.println("数组索引位置是 " + left + " 要查找的只是 " + nums[left]);
        }
    }

    @Test
    public void binarySearchMoreThenOrEqualFirst2() {
        int target = 4;
        int length = nums.length;
        int left = 0;
        int right = length - 1;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid] >= target) {
                if (mid == 0 || nums[mid - 1] < target) {
                    System.out.println("数组索引位置是 " + mid + " 要查找的只是 " + nums[mid]);
                }
                right = mid - 1;
            }else{
                left = mid + 1;
            }
        }
    }


    //=====================================
    //查找最后一个小于等于给定值的元素
    //处理方式同 binarySearchLast1 相同
    @Test
    public void binarySearchLessThenOrEqual1() {
        int target = 6;
        int length = nums.length;
        int left = 0;
        int right = length - 1;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid] <= target) {
                left = mid + 1;
            }else{
                right = mid - 1;
            }
        }
        if (right >= 0 && nums[right] <= target) {
            System.out.println("数组索引位置是 " + right + " 要查找的是 " + nums[right]);
        }
    }


    @Test
    public void binarySearchLessThanOrEqual2() {
        int target = 6;
        int length = nums.length;
        int left = 0;
        int right = length - 1;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (nums[mid] <= target) {
                if (mid == length - 1 || nums[mid + 1]<target) {
                    System.out.println("数组索引位置是 " + mid + " 要查找的是 " + nums[mid]);
                }
                left = mid + 1;
            }else{
                right = mid - 1;
            }
        }
    }



}
